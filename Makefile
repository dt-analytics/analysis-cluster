.PHONY: assets jupyter build

# Versão da Plataforma
OBSD_VERSION=1.0

# Versões das aplicações
SPARK_VERSION=3.3.1
HADOOP_MAJOR_VERSION=3

# Principais URLs
APACHE_ARCHIVE=https://archive.apache.org
SPARK_ARCHIVE=$(APACHE_ARCHIVE)/dist/spark/spark-$(SPARK_VERSION)
MINICONDA_URL=https://repo.anaconda.com/miniconda/Miniconda3-py39_4.12.0-Linux-x86_64.sh

###############################################
# Download dos principais ativos (Spark, ...) #
###############################################
assets:
	@mkdir --parents assets/archive
	@echo "Downloading Apache Spark"
	curl --remote-name --location $(SPARK_ARCHIVE)/spark-$(SPARK_VERSION)-bin-hadoop$(HADOOP_MAJOR_VERSION).tgz
	curl --remote-name --location $(SPARK_ARCHIVE)/spark-$(SPARK_VERSION)-bin-hadoop$(HADOOP_MAJOR_VERSION).tgz.sha512	
	for arq in *.sha512; do cat $${arq} | sha512sum --check ; done
	mv spark-$(SPARK_VERSION)-bin-hadoop$(HADOOP_MAJOR_VERSION).tgz assets/archive/spark.tgz
	rm *.sha512
	@echo "Downloading Miniconda"
	curl --show-error --location $(MINICONDA_URL) --output assets/archive/miniconda.sh

base:
	docker build ./docker/base --tag obsd/base
jupyter: spark
	docker build ./docker/jupyter --tag obsd/jupyter
# spark-base: base
# 	cp assets/archive/spark.tgz docker/spark/base
# 	docker build ./docker/spark/base --tag obsd/spark-base
# 	rm docker/spark/base/spark.tgz
# spark-master: spark-base
# 	docker build ./docker/spark/master --tag obsd/spark-master
# spark-worker: spark-base
# 	docker build ./docker/spark/worker --tag obsd/spark-worker

run-jupyter:
	docker run --rm -it -v "$$PWD/assets/notebooks/":/home/jovyan/notebooks -v "$$PWD/assets/scripts/":/home/jovyan/scripts -v "$$PWD/assets/data/:/home/jovyan/data" -p 8888:8888 obsd/jupyter

spark: base
	cp assets/archive/spark.tgz docker/spark
	docker build ./docker/spark --tag obsd/spark
	rm docker/spark/spark.tgz

build:
	docker compose build

up: jupyter spark
	docker compose --env-file env/develop up

down: compose.yaml
	docker compose --env-file env/develop down
