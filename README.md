# Data Analytics 

<!--
*** Written Victor Santos <vct.santos@protonmail.com>
*** https://vsantos.me/
-->
![setup of the platform][arch]

This setup aims to provide a simple platform for data analysis, based on the following services:
- Exploration, transformation and analysis of data using [Jupyter Lab](https://jupyter.org/), with [R](https://www.r-project.org/) and [Python](https://www.python.org/);
- Manage the workload of the analysis with [Apache Spark](https://spark.apache.org/).

## Requisites

- [Docker](https://www.docker.com/) (with [compose](https://docs.docker.com/compose/) enabled);
- [GNU Make](https://www.gnu.org/software/make/) (*optional*, for automated building of the images and services).

## Starting the services

You can run `make up` to build the images and start the services.

### Accessing the services

- Spark: access master node interface at http://localhost:8080
- Jupyter: acess web interface at http://localhost:8888

<!-- Links do Markdown & Imagens -->
[arch]: assets/img/arch.png
